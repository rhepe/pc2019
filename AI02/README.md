# Agile Integration for Developers

#### This lab with give you a hands on run through of Openshift, Fuse Online, AMQ and 3scale. Working from the point of view of a developer, you will build out your APIs, Integrate using Fuse Online, then use 3scale to secure your APIs

<img src="profile.jpg">

 Eoin Crosbie,
 Technical Partner Enablement Manager, Agile Integration

 ecrosbie@redhat.com

Lab schedule
 - Tuesday, 16:00


| What | Where |
|---|---|
| To get your GUID and userId and access the Lab, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
| The Lab Guide can be found | https://tutorial-web-app-webapp.apps.GUID.events.opentlc.com/ |
| The Lab source code is located here | https://github.com/Crosbie/tutorial-web-app-walkthroughs |



