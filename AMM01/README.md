# RHAMT (Red Hat Application Migration Toolkit) on Openshift

### Learn about RHAMT, the tool by Red Hat which helps you in analyzing your existing brownfield and gives you a defined list of items, tasks, technical details and measurement about the effort needed to migrate wour java enterprise based workload to the Red Hat technologies.
<img src="profile_pic.jpg">

 Andrea Battaglia, 
 EMEA Partner enablement Manager - Digital Transformation and Cloud Native App Dev
 
 Middleware Evangelist
 
 andrea.battaglia@redhat.com


Lab schedule
 - Tuesday, 14:00


| What | Where |
|---|---|
| The presentation to the Lab can be found |  [Lab Presentation](https://gogs.rhepds.com/PC2019/AMM01/raw/master/PC19%20AMM01%20-%20Presentation.pdf) |
| To get the GUID for your lab go | [Here](https://www.opentlc.com/gg/gg.cgi?profile=generic_pc) |
| The Lab Documentation is localted here | [Lab Guide](https://gogs.rhepds.com/PC2019/AMM01/raw/master/PC19%20-%20AMM01%20-%20Lab%20Guide.pdf) |

