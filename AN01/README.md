# AN01 
## Getting Started with Ansible

Ansible is a simple yet powerful IT automation engine for app deployment, configuration management and orchestration that you can learn quickly. In this lab, after a brief introduction, you'll install Ansible and run the first commands. Next, we'll tackle some of the basic concepts and you'll start to write your first playbooks. Along the way, you'll learn more advanced concepts, such as controlling task execution and templating.

| Goetz Rieger | Roland Wolters |
|---|---|
| <img src="grieger.jpg">  | <img src="roland1.jpg"> |
| Senior Solution Architect | Technical marketing Manager |
| grieger (at) redhat (dot) com | rwolters (at) redhat (dot) com |

Lab schedule
 - Wednesday, 09:00, Room 221


| What | Where |
|---|---|
| To get your GUID and access the Lab, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
| The Lab Documentation is localted here | https://github.com/goetzrieger/ansible-labs/blob/master/engine/getting_started_ansible.adoc |


