# AN04
## Automate your Network with Ansible

Introduction to Ansible for network engineers and operators

| Roland Wolters | Andreas Stolzenberger |
|---|---|
|  <img src="roland1.jpg"> | <img src="andi1-hat-300.jpg">  |
| Technical marketing Manager | technical Partner Enablement Manager |
| rwolters (at) redhat (dot) com | ast (at) redhat (dot) com |

Lab schedule
 - Wednesday, 09:00, Room 221


| What | Where |
|---|---|
| The Lab Documentation is localted here | http://ansible-emea.events.opentlc.com/ |


