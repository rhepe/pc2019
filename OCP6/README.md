# OCP6
## Developer Session: Introducing the Operator Framework hands-on 

To help make it easier to build Kubernetes applications, Red Hat and the Kubernetes open source community developer the Operator Framework– an open source toolkit designed to manage Kubernetes native applications, called Operators, in a more effective, automated, and scalable way. In this lab, we’ll learn about the Operator Framework, introduce the SDK, Lifecycle Management and Metering.

<img src="wpernath.jpeg">

_Wanja Pernath, EMEA Partner Enablement Manager Middleware & OpenShift, Red Hat_

Lab schedule
 - Wednesday, 14:00
 


| What | Where |
|---|---|
| The presentation to the Lab can be found | [here](PC2019_BuildYouOwnOperator.pdf) |
| URL to start lab | [here](https://www.opentlc.com/gg/gg.cgi?profile=generic_pc)|
| Log into the Cluster | oc login -u admin -p r3dh4t1! |


