# The Infrastructure Migration Toolkit

### How to easyly migrate your existing VMs from an expensive legacy Environments (like Vsphere) to Red Hats open and future proof Plattforms like RHV or Openstack

| Miguel Perez Colino| Amaya Rosa Gil Pippino  |
|---|---|
| <img src="miguel.jpg">  | <img src="amaya.jpg"> |
| Principal Product Manager | EMEA Senior Solution Architect RHCA Level V |
| miguel (at) redhat (dot) com | amaya (at) redhat (dor) com |


Lab schedule
 - Tuesday, 10:30, Room 223 
 

| What | Where |
|---|---|
| The presentation to the Lab can be found | [ Presentation PDF ]( ims_pc2019.pdf )  |
| To get your GUID and access the Lab, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
| The Lab Documentation is localted here |http://bit.ly/Migrate-Lab |


