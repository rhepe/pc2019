# Combined SAP and Insights Lab

## This is actually two labs following each other.
#### In the first lab you will learn how to instrument Red Hat Ansible roles to deploy SAP HANA quick and easily.
[Click here for Dokumenation of SAP Lab](https://gogs.rhepds.com/PC2019/SAP01/src/master/SAP01.md)
#### The second lab will show you the values of Red Hat Insights using the previously installed HANA systems as an example workload.
[Click here for Documentation of the Insights Lab](https://github.com/amayagil/InsightsSAPLab)

#### *NOTE:* It is possible to attend to each of these labs seperately

#### [Click here to get access credentials for your LAB](https://www.opentlc.com/gg/gg.cgi?profile=generic_pc)

## Lab schedule
 - [Thursday, 13:30, Lab Room 4](https://gogs.rhepds.com/PC2019/SAP01/src/master/SAP01.md)
 - [Thursday, 15:00, Lab Room 4](https://github.com/amayagil/InsightsSAPLab)
