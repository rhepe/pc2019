# Deploy SAP HANA with Ansible Roles

### This lab demonstrates how to use Ansible Roles to deploy SAP HANA systems.
<img src="mkoch.jpg">

 Markus Koch, 
 EMEA Partner enablement Manager  
 mkoch (ät) redhat (dot) com

Lab schedule
 - Thursday, 13:30, Lab 4


| What | Where |
|---|---|
| The presentation to the Lab can be found | [Internal](https://docs.google.com/presentation/d/1eLpjCt-HQ491WICllbTdQpS__glLpl5qbS2djBmfhpI/edit?usp=sharing) - [External](https://gogs.rhepds.com/PC2019/SAP01/src/master/presentation/PC19%20SAP01%20-%20SAP%20Ansible%20Roles.pdf) |
| To get your GUID and access the Lab, click | [here](https://www.opentlc.com/gg/gg.cgi?profile=generic_pc) |
| The Lab Documentation is located here |  [HTML](https://people.redhat.com/mkoch/training/pc2019/labguide) - [Markdown](https://gogs.rhepds.com/PC2019/SAP01/src/master/labguide/README.md)|


