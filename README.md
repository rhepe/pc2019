# PC2019 Documentation Repositories

This Repo contains all Docs that were used during the Lab-Sessions of the EMEA Partner Conference 2019 in Prague. Please report missing Documents or non functioning links, since the Repositories have been moved to this Gitlab environment from another URL.

