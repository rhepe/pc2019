# OCP5
## Use OpenShift DO to streamline the developer experience 

OpenShift Do, or ODO is a developer-centric CLI tool that opens up continuous development for OpenShift. ODO makes it easier to communicate with OpenShift via command line as a developer. It hides a lot of complexity of dealing with object creation and modification and other tasks. In this lab, we’ll get hands-on and experience the power the ODO brings the developer.


<img src="wpernath.jpeg" >

_Wanja Pernath, EMEA Partner Enablement Manager Middleware & OpenShift, Red Hat_

Lab schedule
 - Wednesday, 09:00
 


| What | Where |
|---|---|
| The presentation to the Lab can be found | [here](presentation.pdf) |
| To get your GUID and access the Lab, click | [here](https://www.opentlc.com/gg/gg.cgi?profile=generic_pc) |


## Lab Instructions

* Click on GUID page to request a User ID
* Remember the User ID 
* Click on the URL of "Project Name" 
* Your User Name: user<ID>
* Password: r3dh4t1!
* Once the Jupyter is started the first thing you should do is:
* oc login https://master.c93a.events.opentlc.com/ -u userID -p r3dh4t1!

And then just run through the course.


