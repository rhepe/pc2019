# AN07 Enhance the Power of Cloudforms with the inbuilt Ansible Features


| Christian Jung, Principal Specialist Solution Architect, Red Hat | Andrés Valero, CloudForms Specialist Solution Architect, Red Hat |
|---|---|
| <img src="cjung.jpg"> | <img src="avaleror.jpg"> |


Have you heard about the embedded Ansible theme of Red Hat CloudForms? Keen to learn more? Join us in this lab session for hands-on exercises utilizing Red Hat Ansible Automation in CloudForms.

In this lab, we'll start with a brief overview presentation. Then, you will configure your CloudForms instance for embedded Ansible Automation and add a playbook repository. Next, you will learn how to use Ansible Playbooks to create service catalog items for the CloudForms self-service portal. Finally, you will use Ansible Playbooks as actions in CloudForms Policies and finally learn how playbooks can be run from custom buttons.

## Lab schedule

Thursday, 15 - 16.30, Room 220

-----

| What | Where |
|---|---|
| To access the Lab, | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
| The Lab Documentation is located here: | [https://gitlab.com/cjung/partner-conference-2019/blob/master/cloudforms-and-ansible-deep-dive/lab/index.md](https://gitlab.com/cjung/partner-conference-2019/blob/master/cloudforms-and-ansible-deep-dive/lab/index.md) |
