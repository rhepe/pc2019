# Cloud Native App-Dev with RHOAR on OCP

### Get a comprehensive technical overview of the recommended practices for designing and implementing microservices application architecture based on Container Platform by Red Hat. Test RHOAR+ product and understand the basic of programming using cloud native framework. Use and test CI/CD features provided by Openshift. Step by step hands on guided lab with support of Partner and Red Hat experts, which includes all the technical aspects of the Dev side of a project. Cloud Dev environment which detaches every dev from her/his development machine.

<img src="profile_pic.jpg">

 Andrea Battaglia, 
 EMEA Partner enablement Manager - Digital Transformation and Cloud Native App Dev
 
 Middleware Evangelist
 
 andrea.battaglia@redhat.com


Lab schedule
 - Thursday, 13:30


| What | Where |
|---|---|
| The presentation to the Lab can be found | [Lab Presentation](https://gogs.rhepds.com/PC2019/CN01/raw/master/PC2019%20CN01%20-%20Presentation.pdf) |
| To get the id for your user go | [Here](https://www.opentlc.com/gg/gg.cgi?profile=generic_pc) |
| The Lab Documentation is localted here | [Lab Guide](http://guides-che-lab-infra.apps.9fe9.events.opentlc.com/workshop/cloudnative/lab/getting-started-che) |

