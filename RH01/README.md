# What's new in RHEL8

### The next Generation of Red Hat Enterprise Linux is here. Learn about new and enhanced features and functions. Get Hands-On with the even more powerful Web-UI Cockpit and all its rich features like Composer and VM Management.
<img src="andi1-hat-300.jpg">

 Andreas Stolzenberger, 
 EMEA Partner enablement Manager & technical Team Lead 
 ast (ät) redhat (dot) com

Lab schedule
 - Tuesday, 14:00, Room 220
 - Wednesday, 14:00, Room 221


| What | Where |
|---|---|
| The presentation to the Lab can be found | [ Presentation PDF ](rhel8_pc2019.pdf) |
| To get your GUID and access the Lab, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
| The Lab Documentation is localted here | https://github.com/xtophd/RHEL8-Workshop/blob/master/documentation/RHEL8-Workshop.adoc |


