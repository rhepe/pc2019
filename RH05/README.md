# Getting Started with Red Hat Virtualization

### How-to easily setup and manage your Red Hat Virtualization environments.

| Dennis Deitermann | Joachim Schröder |
|---|---|
| <img src="dennis.jpg">  | <img src="jos.jpg"> |
| Senior Solution Architect | Manager Solution Architects Germany |
| dennis (at) redhat (dot) com | jschrode (at) redhat (dor) com |

Lab schedule
 - Wednesday, 09:00, Room 220


| What | Where |
|---|---|
| To get your GUID and the labguide, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
