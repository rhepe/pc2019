# AN03 
## Advanced Ansible Tower

You started using Ansible Tower, already love it and want to know more? Then this lab is for you. You have already covered the basics, now we'll give you a hands-on introduction to the more advanced concepts and features. You'll start with learning and exploring Ansible Tower clustering and how to use it for high-availability and load-balancing. From here the next step is to learn about the isolated node feature which allows to automate hosts in separate networks with limited access. You already know about inventories, let's step it up a bit and introduce you to dynamic inventories and the smart inventory feature. Along the way you'll learn how to organize your Ansible roles and content in well-structured Git repositories. Finally we'll cover how to use the API to expose even more ways to leverage Towers power!

<img src="grieger.jpg">
_Goetz Rieger, Senior Solution Architect, Red Hat_

Lab schedule
 - Wednesday, 14:00, Lab 4
 - Thursday, 09:00, Lab 3
 


| What | Where |
|---|---|
| The presentation to the Lab can be found | [here](https://gogs.rhepds.com/PC2019/AN03/src/master/Advanced%20Ansible%20Tower%20-%20EMEA%20PC%202019.pdf) |
| To get your GUID and access the Lab, click | https://bit.ly/2X1XMlc |
| The Lab Documentation is localted here | https://people.redhat.com/grieger/emeapc2019_labs/ansible_tower_for_advanced_users.html |


