# OpenShift Application Deployment and Lifecycle Management

### In this Lab you will get additional hands-on introduction to the OpenShift Container Platform. You will learn how to do advanced deployment techniques and application lifecycle management.

| Dennis Deitermann | Daniel Brintzinger |
|---|---|
| <img src="dennis.jpg">  | <img src="daniel.jpg"> |
| Senior Solution Architect | Senior Solution Architect |
| dennis (at) redhat (dot) com | dbrintzi (at) redhat (dor) com |

Lab schedule
 - Wednesday, 16:00, Room 222  
 - Thursday, 10:30, Room 220


| What | Where |
|---|---|
| To get your GUID and the labguide, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |