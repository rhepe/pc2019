# Getting Started with Red Hat IdM

### Red Hat Identity Management (IDM) can play a central role in user authentication, authorization and control. It can manage critical security components such as ssh keys, host based access controls and selinux contexts. These functions can be provided in a standalone environment or in a trust relationship with a Microsoft Active Domain Controller.

| Dennis Deitermann | Joachim Schröder |
|---|---|
| <img src="dennis.jpg">  | <img src="jos.jpg"> |
| Senior Solution Architect | Manager Solution Architects Germany |
| dennis (at) redhat (dot) com | jschrode (at) redhat (dor) com |

Lab schedule
 - Tuesday, 16:00, Lab 2  
 - Thursday, 15:00, Lab 3


| What | Where |
|---|---|
| To get your GUID and the labguide, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |