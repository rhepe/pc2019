# RH02 
## Hands on with Red Hat Satellite 6.5

In this lab, you'll be exposed to the latest version of Red Hat Satellite, which is 6.5. You'll be able to exercise new features and see how Satellite improves the management of Red Hat Enterprise Linux.


| Ivan Necas| Ondrej Ezr |
|---|---|
| <img src="inecas.jpg">  | <img src="ondrej.jpg"> |
| Principal Software Engineer, Red Hat | Software Engineer Satellite 6.x |
| inecas (at) redhat (dot) com | oezr (at) redhat (dot) com |


Lab schedule
 - Tuesday, 16:00, Lab 223
 - Wednesday, 16:00, Lab 221
 


| What | Where |
|---|---|
| To get your GUID and access the Lab, click | https://www.opentlc.com/gg/gg.cgi?profile=generic_pc |
| The Lab Documentation is localted here | https://redhatsummitlabs.gitlab.io/hands-on-with-satellite-6-5/ |


