# AN02 
## Taking IT-Automation to the next Level with Ansible Tower

Red Hat Ansible Tower helps you centralize and control your IT infrastructure with a visual dashboard, role-based access control, job scheduling, and graphical inventory management. In this lab you'll start with configuring inventories and credentials, then learn how to integrate your playbooks. After configuring job templates, you'll run your first Ansible jobs from Tower. Finally, we'll show you how to give users without Ansible knowledge limited control of playbook execution and introduce you to the workflows feature. This lab is best for people who already have basic experience with Ansible.

<img src="grieger.jpg">
_Goetz Rieger, Senior Solution Architect, Red Hat_

Lab schedule
 - Wednesday, 14:00, Lab 4
 - Thursday, 09:00, Lab 3
 


| What | Where |
|---|---|
| The presentation to the Lab can be found | [here](https://gogs.rhepds.com/PC2019/AN02/src/master/Getting%20started%20with%20Ansible%20Tower%20-%20EMEA%20PC%202019.pdf) |
| To get your GUID and access the Lab, click | https://bit.ly/2X1XMlc |
| The Lab Documentation is localted here | https://people.redhat.com/grieger/emeapc2019_labs/getting_started_with_red_hat_ansible_tower.html |


